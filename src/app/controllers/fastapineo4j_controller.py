from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.repository.fastapineo4j_repo import FastAPINeoRepo
from app.service.fastapineo4j_command import FastAPINeoService

router=InferringRouter()

@cbv(router=router)
class FastAPINeoController:
    neo_service:FastAPINeoService=Depends(FastAPINeoService)

    @router.post(
        "/view",
        name="View Data"
    )
    def get(self):
        return self.neo_service.view()

