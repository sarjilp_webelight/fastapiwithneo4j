from fastapi_utils.api_model import APIModel


class ResponseModel(APIModel):
    """
    Response model.
    """
    data: str
