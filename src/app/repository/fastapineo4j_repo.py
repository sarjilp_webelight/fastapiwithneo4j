from fastapi import Depends

from core.db.session import connection

class FastAPINeoRepo:
     def __init__(self,db:connection=Depends(connection)):
         self.session=db


     def view(self):
         cypher_query="""
         MATCH (n)
         RETURN n
         """
         answer=self.session.run(cypher_query)
         return answer