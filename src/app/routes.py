from app.controllers.fastapineo4j_controller import router as neo_router
from fastapi import APIRouter

router = APIRouter()
router.include_router(neo_router)

__all__ = ["router"]