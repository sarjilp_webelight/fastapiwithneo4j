from app.routes import router
from config import conf
from fastapi import FastAPI
from starlette import status
from starlette.middleware.cors import CORSMiddleware

from app import constants
from app.models import ResponseModel


def init_middleware(_app: FastAPI) -> None:
    """
    Middleware initialization.
    """
    _app.add_middleware(
        CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"]
    )


def init_routers(_app: FastAPI) -> None:
    """
    Initialize all routers.
    """
    _app.include_router(router, responses={status.HTTP_422_UNPROCESSABLE_ENTITY: {"model": ResponseModel}})


def create_app() -> FastAPI:
    """
    Master function for creating the :class:`FastAPI` APP.

    :return: A FastAPI instance.
    """
    _app = FastAPI(
        title=conf.PROJECT_NAME,
        description=constants.DESCRIPTION,
        #version=conf.VERSION,
        debug=conf.env().get("is_env_local"),
        docs_url=conf.env().get("docs_url"),
        redoc_url=conf.env().get("redoc_url"),
        reload=False,
    )
    init_routers(_app=_app)
    init_middleware(_app=_app)
    return _app


app = create_app()


@app.on_event("startup")
async def startup():
    """
    Startup event.
    """
    pass


@app.on_event("shutdown")
async def shutdown():
    print("Shutting down...")  # noqa T201
