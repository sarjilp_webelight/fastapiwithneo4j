from fastapi import Depends

from app.repository.fastapineo4j_repo import FastAPINeoRepo

class FastAPINeoService:
    def __init__(self,neo_repo:FastAPINeoRepo=Depends(FastAPINeoRepo)):
        self.neo_repo=neo_repo

    def view(self):
        return self.neo_repo.view()