import os
from enum import Enum
from typing import Union

from dotenv import load_dotenv
from pydantic import BaseSettings


def load_env():
    """
    Get the env file path.
    """
    # look for .env file in parent hierarchy of the project
    # (useful for local development)
    current_depth = 0  # current depth in the hierarchy
    max_depth = 3  # maximum depth to look for .env file
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    while current_depth < max_depth:
        file = os.path.join(root, ".env")
        if os.path.exists(file):
            load_dotenv(file)
            return str(file)
        root = os.path.dirname(root)
        current_depth += 1


load_env()


class AppEnvironment(str, Enum):
    """
    An Enum class defining the development environments.
    """

    LOCAL = "LOCAL"
    DEV = "DEV"
    PROD = "PROD"
    TEST = "TEST"


class Settings(BaseSettings):
    """
    A settings class for the project defining all the necessary parameters within the
    app through a object.
    """

    BASE_DIR: str = os.path.abspath(os.path.pardir)
    PROJECT_NAME: str = "FastAPI with Neo4j"
    VERSION: str = "1.0.0"
    HOST: str = os.environ.get("FASTAPI_NEO4J_HOST", "0.0.0.0")
    PORT: int = int(os.environ.get("FASTAPI_NEO4J_PORT", 8086))
    ENV: AppEnvironment = os.environ.get("ENV", "LOCAL")

    NEO4J_URI: str = os.environ.get("NEO_DB_URI")
    NEO4J_USER: str = os.environ.get("NEO_DB_USER")
    NEO4J_PASSWORD: str = os.environ.get("NEO_DB_PASSWORD")

    class Config:
        """
        Configuration for the pydantic :class:`BaseSettings`
        """

        case_sensitive = True

    def env(self) -> dict[str, Union[bool, str]]:
        """
        The environment method to fetch necessary environment variables.

        :return: A dictionary with environment variables.
        """
        if self.ENV == AppEnvironment.PROD:
            return {"is_env_local": False, "redoc_url": "", "docs_url": ""}
        else:
            return {"is_env_local": True, "redoc_url": "/redoc", "docs_url": "/docs"}


conf = Settings()
