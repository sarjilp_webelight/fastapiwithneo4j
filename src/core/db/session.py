from config import conf
from neo4j import GraphDatabase

driver = GraphDatabase.driver(uri=conf.NEO4J_URI, auth=(conf.NEO4J_USER, conf.NEO4J_PASSWORD))


def connection():
    """
    creating neo4j driver for connection with neo4j.
    """
    return driver.session()
