import uvicorn
from config import AppEnvironment, conf


def main():
    """
    Master main file to initialize the Authentication Server.
    """
    uvicorn.run(
        app="app.server:app",
        host=conf.HOST,
        port=conf.PORT,
        reload=False if conf.ENV == AppEnvironment.PROD else True,
        workers=1,
    )


if __name__ == "__main__":
    main()
